#!/usr/bin/python
from PIL import Image, ImageDraw
from pprint import pprint
import numpy as np

imageAscending = Image.open("images/ascending.png")
imageDescending = Image.open("images/descending.png")
imageWavy = Image.open("images/wavy.png")

processedImage = imageAscending
processedWidth, processedHeight = processedImage.size
##################################################
def imgToList(image):
    listOfCoords = []
    width, height = image.size
    for idx_i in range(width):
        for idx_j in range(height):
            if (image.getpixel((idx_i, idx_j)) == 0):
                listOfCoords.append([idx_i, idx_j])
    return listOfCoords
##################################################

def linearRegression(dataSet):
    Q = 0
    # 1) Calculate mean x and mean y
    sum_x, sum_y = 0, 0
    for a_point in dataSet:
        sum_x += a_point[0]
        sum_y += a_point[1]
    avg_x, avg_y = sum_x/len(dataSet), sum_y/len(dataSet)
    print "mid_x, mid_y:", avg_x, avg_y

    # 2) calculate a and b parameters
    num_frac_b, den_frac_b = 0, 0
    for a_point in dataSet:
        num_frac_b += (a_point[0]-avg_x)*(a_point[1]-avg_y)
        den_frac_b += pow((a_point[0]-avg_x), 2)
    b = num_frac_b/float(den_frac_b)
    b = int(b*1000)/1000.0
    print "b=", b
    a = avg_y - b * avg_x
    print "a=", a

    imageResultColoured = Image.new('RGB', processedImage.size, (255, 255, 255))
    draw = ImageDraw.Draw(imageResultColoured)

    for an_existing_point in dataSet:
        # Color the final, result image
        Q +=pow((an_existing_point[1]-a-b*an_existing_point[0]), 2)
        imageResultColoured.putpixel((an_existing_point[0], an_existing_point[1]), (0,0,0))


    for idx_i in range(processedWidth):
        for idx_j in range(processedHeight):
            if 0.99*idx_j < a + b * idx_i < 1.001*idx_j:
              imageResultColoured.putpixel((idx_i, idx_j), (255, 0, 0))

    print "Q(a,b)=", Q
    print "\n"

    imageResultColoured.show()



### Main ###
linearRegression(imgToList(processedImage))

processedImage = imageWavy
processedWidth, processedHeight = processedImage.size
linearRegression(imgToList(processedImage))

processedImage = imageDescending
processedWidth, processedHeight = processedImage.size
linearRegression(imgToList(processedImage))

# pprint (dataSet)
